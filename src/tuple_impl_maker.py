def generate_tuple(n):
    return "impl<" +\
    ',\n'.join("A{}: PredicateArg".format(i) for i in range(n)) +\
    "> PredicateArg for ("+\
    ', '.join("A{}".format(i) for i in range(n)) +\
    """) {
    fn merge(&self, other: Self) -> Option<Self> {
        Some((\n""" +\
        ', \n'.join("            self.{}.merge(other.{})?".format(i, i) for i in range(n)) +\
    """
        ))
    }
}\n"""

if __name__ == "__main__":
    import sys
    for n in range(2, 1+int(sys.argv[1])):
        print(generate_tuple(n))