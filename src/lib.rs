mod tuples;
pub mod predicates;
pub mod predicate_arguments;
pub use predicate_arguments::PredicateArg;
use predicates::*;
pub use predicates::Translator;

type Explorer<'l, Arg> = Box<dyn Iterator<Item = Arg> + 'l>;

pub trait Predicate<'a, Arg: PredicateArg + 'a>: std::fmt::Debug {
    fn explore<'b>(&'b self, arg: Arg) -> Explorer<'b, Arg> where Arg: PredicateArg + 'b, 'a: 'b;
}

pub enum P<'l, Arg> {
    True,
    Constraint(Constraint<Arg>),
    Or(Or<P<'l, Arg>>),
    And(And<P<'l, Arg>>),
    Boxed(Box<dyn Predicate<'l, Arg> + 'l>)
}
impl<'l, Arg: PredicateArg + 'l> P<'l, Arg> {
    pub fn new() -> Self {
        P::True
    }
    pub fn constraint(arg: Arg) -> Self {
        P::Constraint(Constraint::new(arg))
    }
    pub fn or(self, other: Self) -> Self {
        match (self, other) {
            (P::True, _) | (_, P::True) => P::True,
            (P::Or(mut l), P::Or(r)) => {
                l.alternatives.extend(r.alternatives.into_iter());
                P::Or(l)
            }
            (P::Or(l), r) => P::Or(l.or(r)),
            (l, P::Or(r)) => P::Or(r.or(l)),
            (l, r) => P::Or(Or::new(l).or(r)),
        }
    }
    pub fn and(self, other: Self) -> Self {
        match (self, other) {
            (P::True, p) | (p, P::True) => p,
            (P::And(mut l), P::And(r)) => {
                l.predicates.extend(r.predicates.into_iter());
                P::And(l)
            }
            (P::And(l), r) => P::And(l.and(r)),
            (l, P::And(r)) => P::And(r.and(l)),
            (l, r) => P::And(And::new(l).and(r)),
        }
    }
    pub fn boxed<T: Predicate<'l, Arg> + 'l>(p: T) -> Self {
        P::Boxed(Box::new(p))
    }
    pub fn function<T: Predicate<'l, O> + 'l, O: PredicateArg + std::any::Any>(pred: T, translator: Translator<'l, Arg, O>) -> Self where Arg: std::any::Any {
        P::Boxed(Function::new(pred, translator).boxed())
    }
}
impl<'a, Arg: PredicateArg + 'a> Predicate<'a, Arg> for P<'a, Arg> {
    fn explore<'b>(&'b self, arg: Arg) -> Explorer<'b, Arg> where Arg: PredicateArg + 'b, 'a: 'b {
        match self {
            P::True => True.explore(arg),
            P::Constraint(p) => p.explore(arg),
            P::Or(p) => p.explore(arg),
            P::And(p) => p.explore(arg),
            P::Boxed(p) => p.explore(arg)
        }
    }
}

impl<'l, Arg: std::fmt::Debug> std::fmt::Debug for P<'l, Arg> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            P::True => True.fmt(f),
            P::Constraint(p) => p.fmt(f),
            P::Or(p) => p.fmt(f),
            P::And(p) => p.fmt(f),
            P::Boxed(p) => {p.fmt(f)}
        }
    }
}

#[cfg(test)]
mod test {
    use crate::*;
    use Test::*;
    #[derive(Copy, Clone, Eq, PartialEq)]
    enum Test<'l> {
        Unknown,
        Known(&'l str),
    }
    impl<'l> std::fmt::Debug for Test<'l> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            match self {
                Unknown => write!(f, "_"),
                Known(s) => write!(f, "{}", s),
            }
        }
    }
    impl<'l> crate::PredicateArg for Test<'l> {
        fn merge(self, other: Self) -> Option<Self> {
            match (self, other) {
                (Unknown, a) => Some(a),
                (a, Unknown) => Some(a.clone()),
                (Known(a), Known(b)) if a == b => Some(Known(b)),
                _ => None,
            }
        }
    }

    fn match_iters<T: Eq + Clone + std::fmt::Debug, I: IntoIterator<Item = T>>(l: I, r: &[T]) {
        let mut positions = Vec::new();
        for l in l {
            if let Some(p) = r.iter().position(|r| r == &l) {
                positions.push(p);
            } else {
                panic!("Unexpected resolution: {:?}", l)
            }
        }
        let misses: Vec<T> = r
            .iter()
            .enumerate()
            .filter_map(|(i, r)| {
                if positions.iter().position(|n| i == *n).is_none() {
                    Some(r.clone())
                } else {
                    None
                }
            })
            .collect();
        if !misses.is_empty() {
            panic!("Missed the following cases: {:?}", misses)
        }
    }

    #[test]
    fn constraint() {
        let p = P::constraint((Unknown, Known("i32")));
        match_iters(
            p.explore((Known("f64"), Unknown)),
            &[(Known("f64"), Known("i32"))],
        );
        match_iters(p.explore((Unknown, Unknown)), &[(Unknown, Known("i32"))]);
    }

    #[test]
    fn or() {
        let p = P::constraint((Unknown, Known("i32"), Unknown)).or(P::constraint((
            Unknown,
            Unknown,
            Known("f64"),
        )));
        match_iters(
            p.explore((Known("&str"), Unknown, Unknown)),
            &[
                (Known("&str"), Known("i32"), Unknown),
                (Known("&str"), Unknown, Known("f64")),
            ],
        );
    }

    #[test]
    fn and() {
        let p = P::constraint((Unknown, Known("i32"), Unknown)).and(
            P::constraint((Unknown, Unknown, Known("f64"))).or(P::constraint((
                Unknown,
                Unknown,
                Known("i32"),
            ))),
        );
        match_iters(
            p.explore((Known("&str"), Unknown, Unknown)),
            &[
                (Known("&str"), Known("i32"), Known("f64")),
                (Known("&str"), Known("i32"), Known("i32")),
            ],
        );
    }

    #[test]
    fn function() {
        let p = P::constraint((Unknown, Known("i32"), Unknown)).and(P::function(P::constraint(Known("&str")), Translator::new( |(a,_,_)|a,  |a| (a, Unknown, Unknown))));
        match_iters(
            dbg!(p).explore((Unknown, Unknown, Known("f64"))),
            &[
                (Known("&str"), Known("i32"), Known("f64")),
            ],
        );
    }
}
