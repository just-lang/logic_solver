pub trait PredicateArg: Sized + Clone + std::fmt::Debug {
    fn merge(self, other: Self) -> Option<Self>;
}
