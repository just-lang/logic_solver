use std::{any::Any, fmt::Debug};

use crate::*;
#[derive(Copy, Clone, Debug)]
pub struct True;
impl<'a, Arg: PredicateArg + 'a> Predicate<'a, Arg> for True {
    fn explore<'b>(&'b self, arg: Arg) -> Explorer<'b, Arg>
    where
        Arg: PredicateArg + 'b,
        'a: 'b,
    {
        Box::new(Some(arg).into_iter())
    }
}

#[derive(Clone)]
pub struct Constraint<Arg> {
    pub(crate) arg: Arg,
}

impl<Arg> Constraint<Arg> {
    pub fn new(arg: Arg) -> Self {
        Constraint { arg }
    }
}

impl<Arg: std::fmt::Debug> std::fmt::Debug for Constraint<Arg> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Constraint({:?})", self.arg)
    }
}

impl<'a, Arg: PredicateArg + 'a> Predicate<'a, Arg> for Constraint<Arg> {
    fn explore<'b>(&'b self, arg: Arg) -> Explorer<'b, Arg>
    where
        Arg: PredicateArg + 'b,
        'a: 'b,
    {
        Box::new(self.arg.clone().merge(arg).into_iter())
    }
}

#[derive(Clone)]
pub struct Or<P> {
    pub(crate) alternatives: Vec<P>,
}

impl<Arg: std::fmt::Debug> std::fmt::Debug for Or<Arg> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Or {:?}", self.alternatives)
    }
}

impl<P> Or<P> {
    pub fn new(p: P) -> Self {
        Or {
            alternatives: vec![p],
        }
    }
    pub fn or(mut self, p: P) -> Self {
        self.alternatives.push(p);
        self
    }
}

impl<'a, Arg: PredicateArg + 'a, P: Predicate<'a, Arg>> Predicate<'a, Arg> for Or<P> {
    fn explore<'b>(&'b self, arg: Arg) -> Explorer<'b, Arg>
    where
        Arg: PredicateArg + 'b,
        'a: 'b,
    {
        Box::new(
            self.alternatives
                .iter()
                .map(move |p| p.explore(arg.clone()))
                .flatten(),
        )
    }
}

#[derive(Clone)]
pub struct And<P> {
    pub(crate) predicates: Vec<P>,
}

impl<Arg: std::fmt::Debug> std::fmt::Debug for And<Arg> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "And {:?}", self.predicates)
    }
}

impl<P> And<P> {
    pub fn new(p: P) -> Self {
        And {
            predicates: vec![p],
        }
    }
    pub fn and(mut self, p: P) -> Self {
        self.predicates.push(p);
        self
    }
}

impl<'a, Arg: PredicateArg + 'a, P: Predicate<'a, Arg>> Predicate<'a, Arg> for And<P> {
    fn explore<'b>(&'b self, arg: Arg) -> Explorer<'b, Arg>
    where
        Arg: PredicateArg + 'b,
        'a: 'b,
    {
        Box::new(AndIter::new(self, arg))
    }
}

struct AndIter<'b, Arg, P> {
    and: &'b And<P>,
    stack: Vec<Explorer<'b, Arg>>,
}
impl<'a: 'b, 'b, Arg, P> AndIter<'b, Arg, P>
where
    Arg: PredicateArg + 'a,
    P: Predicate<'a, Arg>,
{
    fn new(and: &'b And<P>, arg: Arg) -> Self {
        let mut stack = Vec::with_capacity(and.predicates.len());
        stack.push(and.predicates[0].explore(arg));
        AndIter { and, stack }
    }
}

impl<'a: 'b, 'b, Arg: PredicateArg + 'a, P: Predicate<'a, Arg>> Iterator for AndIter<'b, Arg, P> {
    type Item = Arg;

    fn next(&mut self) -> Option<Self::Item> {
        let it = self.stack.last_mut()?;
        let next = it.next();
        match next {
            Some(arg) => {
                let len = self.stack.len();
                if len == self.and.predicates.len() {
                    Some(arg)
                } else {
                    let next = self.and.predicates[len].explore(arg);
                    self.stack.push(next);
                    self.next()
                }
            }
            None => {
                self.stack.pop();
                self.next()
            }
        }
    }
}

pub struct Function<'l, I, O, P> {
    pred: P,
    translator: Translator<'l, I, O>,
}

impl<'l, I: Any, O: Any, P: Debug> Debug for Function<'l, I, O, P> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Function {{{:?} -> {:?}}}", self.translator, self.pred)
    }
}

impl<'a, I: Any, O: Any, P> Function<'a, I, O, P> {
    pub fn new(pred: P, translator: Translator<'a, I, O>) -> Self {
        Function { pred, translator }
    }
    pub fn boxed(self) -> Box<dyn Predicate<'a, I> + 'a>
    where
        P: Predicate<'a, O> + 'a,
        I: PredicateArg + 'a,
        O: PredicateArg + 'a,
    {
        Box::new(self)
    }
}

impl<'a, I: Any, O: Any, P> Predicate<'a, I> for Function<'a, I, O, P>
where
    P: Predicate<'a, O>,
    O: PredicateArg + 'a,
    I: PredicateArg,
{
    fn explore<'b>(&'b self, i: I) -> Explorer<'b, I>
    where
        I: PredicateArg + 'b,
        'a: 'b,
    {
        let o = (self.translator.forward)(i.clone());
        Box::new(
            self.pred
                .explore(o)
                .filter_map(move |o| (self.translator.backward)(o).merge(i.clone())),
        )
    }
}

pub struct Translator<'l, I, O> {
    pub forward: Box<dyn AnyFn<I, O> + 'l>,
    pub backward: Box<dyn AnyFn<O, I> + 'l>,
}
impl<'l, I: Any, O: Any> Translator<'l, I, O> {
    pub fn new(forward: impl AnyFn<I, O> + 'l, backward: impl AnyFn<O, I> + 'l) -> Self {
        Translator {
            forward: Box::new(forward),
            backward: Box::new(backward),
        }
    }
}

impl<'l, I: Any, O: Any> std::fmt::Debug for Translator<'l, I, O> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Translator {{{} <-> {}}}",
            std::any::type_name::<I>(),
            std::any::type_name::<O>()
        )
    }
}

pub trait AnyFn<I: Any, O: Any>: Fn(I)->O {}
impl<I: Any, O: Any, T: Fn(I)->O + Any> AnyFn<I, O> for T {}