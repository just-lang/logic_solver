use crate::PredicateArg;

impl<A0: PredicateArg, A1: PredicateArg> PredicateArg for (A0, A1) {
    fn merge(self, other: Self) -> Option<Self> {
        Some((self.0.merge(other.0)?, self.1.merge(other.1)?))
    }
}

impl<A0: PredicateArg, A1: PredicateArg, A2: PredicateArg> PredicateArg for (A0, A1, A2) {
    fn merge(self, other: Self) -> Option<Self> {
        Some((
            self.0.merge(other.0)?,
            self.1.merge(other.1)?,
            self.2.merge(other.2)?,
        ))
    }
}

impl<A0: PredicateArg, A1: PredicateArg, A2: PredicateArg, A3: PredicateArg> PredicateArg
    for (A0, A1, A2, A3)
{
    fn merge(self, other: Self) -> Option<Self> {
        Some((
            self.0.merge(other.0)?,
            self.1.merge(other.1)?,
            self.2.merge(other.2)?,
            self.3.merge(other.3)?,
        ))
    }
}

impl<A0: PredicateArg, A1: PredicateArg, A2: PredicateArg, A3: PredicateArg, A4: PredicateArg>
    PredicateArg for (A0, A1, A2, A3, A4)
{
    fn merge(self, other: Self) -> Option<Self> {
        Some((
            self.0.merge(other.0)?,
            self.1.merge(other.1)?,
            self.2.merge(other.2)?,
            self.3.merge(other.3)?,
            self.4.merge(other.4)?,
        ))
    }
}

impl<
        A0: PredicateArg,
        A1: PredicateArg,
        A2: PredicateArg,
        A3: PredicateArg,
        A4: PredicateArg,
        A5: PredicateArg,
    > PredicateArg for (A0, A1, A2, A3, A4, A5)
{
    fn merge(self, other: Self) -> Option<Self> {
        Some((
            self.0.merge(other.0)?,
            self.1.merge(other.1)?,
            self.2.merge(other.2)?,
            self.3.merge(other.3)?,
            self.4.merge(other.4)?,
            self.5.merge(other.5)?,
        ))
    }
}

impl<
        A0: PredicateArg,
        A1: PredicateArg,
        A2: PredicateArg,
        A3: PredicateArg,
        A4: PredicateArg,
        A5: PredicateArg,
        A6: PredicateArg,
    > PredicateArg for (A0, A1, A2, A3, A4, A5, A6)
{
    fn merge(self, other: Self) -> Option<Self> {
        Some((
            self.0.merge(other.0)?,
            self.1.merge(other.1)?,
            self.2.merge(other.2)?,
            self.3.merge(other.3)?,
            self.4.merge(other.4)?,
            self.5.merge(other.5)?,
            self.6.merge(other.6)?,
        ))
    }
}

impl<
        A0: PredicateArg,
        A1: PredicateArg,
        A2: PredicateArg,
        A3: PredicateArg,
        A4: PredicateArg,
        A5: PredicateArg,
        A6: PredicateArg,
        A7: PredicateArg,
    > PredicateArg for (A0, A1, A2, A3, A4, A5, A6, A7)
{
    fn merge(self, other: Self) -> Option<Self> {
        Some((
            self.0.merge(other.0)?,
            self.1.merge(other.1)?,
            self.2.merge(other.2)?,
            self.3.merge(other.3)?,
            self.4.merge(other.4)?,
            self.5.merge(other.5)?,
            self.6.merge(other.6)?,
            self.7.merge(other.7)?,
        ))
    }
}

impl<
        A0: PredicateArg,
        A1: PredicateArg,
        A2: PredicateArg,
        A3: PredicateArg,
        A4: PredicateArg,
        A5: PredicateArg,
        A6: PredicateArg,
        A7: PredicateArg,
        A8: PredicateArg,
    > PredicateArg for (A0, A1, A2, A3, A4, A5, A6, A7, A8)
{
    fn merge(self, other: Self) -> Option<Self> {
        Some((
            self.0.merge(other.0)?,
            self.1.merge(other.1)?,
            self.2.merge(other.2)?,
            self.3.merge(other.3)?,
            self.4.merge(other.4)?,
            self.5.merge(other.5)?,
            self.6.merge(other.6)?,
            self.7.merge(other.7)?,
            self.8.merge(other.8)?,
        ))
    }
}

impl<
        A0: PredicateArg,
        A1: PredicateArg,
        A2: PredicateArg,
        A3: PredicateArg,
        A4: PredicateArg,
        A5: PredicateArg,
        A6: PredicateArg,
        A7: PredicateArg,
        A8: PredicateArg,
        A9: PredicateArg,
    > PredicateArg for (A0, A1, A2, A3, A4, A5, A6, A7, A8, A9)
{
    fn merge(self, other: Self) -> Option<Self> {
        Some((
            self.0.merge(other.0)?,
            self.1.merge(other.1)?,
            self.2.merge(other.2)?,
            self.3.merge(other.3)?,
            self.4.merge(other.4)?,
            self.5.merge(other.5)?,
            self.6.merge(other.6)?,
            self.7.merge(other.7)?,
            self.8.merge(other.8)?,
            self.9.merge(other.9)?,
        ))
    }
}

impl<
        A0: PredicateArg,
        A1: PredicateArg,
        A2: PredicateArg,
        A3: PredicateArg,
        A4: PredicateArg,
        A5: PredicateArg,
        A6: PredicateArg,
        A7: PredicateArg,
        A8: PredicateArg,
        A9: PredicateArg,
        A10: PredicateArg,
    > PredicateArg for (A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10)
{
    fn merge(self, other: Self) -> Option<Self> {
        Some((
            self.0.merge(other.0)?,
            self.1.merge(other.1)?,
            self.2.merge(other.2)?,
            self.3.merge(other.3)?,
            self.4.merge(other.4)?,
            self.5.merge(other.5)?,
            self.6.merge(other.6)?,
            self.7.merge(other.7)?,
            self.8.merge(other.8)?,
            self.9.merge(other.9)?,
            self.10.merge(other.10)?,
        ))
    }
}

impl<
        A0: PredicateArg,
        A1: PredicateArg,
        A2: PredicateArg,
        A3: PredicateArg,
        A4: PredicateArg,
        A5: PredicateArg,
        A6: PredicateArg,
        A7: PredicateArg,
        A8: PredicateArg,
        A9: PredicateArg,
        A10: PredicateArg,
        A11: PredicateArg,
    > PredicateArg for (A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11)
{
    fn merge(self, other: Self) -> Option<Self> {
        Some((
            self.0.merge(other.0)?,
            self.1.merge(other.1)?,
            self.2.merge(other.2)?,
            self.3.merge(other.3)?,
            self.4.merge(other.4)?,
            self.5.merge(other.5)?,
            self.6.merge(other.6)?,
            self.7.merge(other.7)?,
            self.8.merge(other.8)?,
            self.9.merge(other.9)?,
            self.10.merge(other.10)?,
            self.11.merge(other.11)?,
        ))
    }
}
